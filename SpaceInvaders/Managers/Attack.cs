﻿using SpaceInvaders.Life;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SpaceInvaders.Managers
{
    class Attack
    {
        public Rectangle AttackImage;
        private BitmapImage AttackVisual;
        private Thread attackThread;
        

        private Canvas _gameCanvas;

        public bool IntersectWith(Rectangle otherRect)
        {
            var SpriteBounds = new System.Drawing.Rectangle((int)Canvas.GetLeft(AttackImage), (int)Canvas.GetTop(AttackImage), (int)AttackImage.Width, (int)AttackImage.Height);
            var OtherBounds = new System.Drawing.Rectangle((int)Canvas.GetLeft(otherRect), (int)Canvas.GetTop(otherRect), (int)otherRect.Width, (int)otherRect.Height);
            return SpriteBounds.IntersectsWith(OtherBounds);

        }

        public Attack(Canvas gameCanvas)
        {
            _gameCanvas = gameCanvas;
            AttackImage = new Rectangle();
            AttackVisual = new BitmapImage(new Uri("pack://application:,,/Assets/fireball.png"));
            attackThread = new Thread(StartAttack);
        }
        public void UseAttack(Player player)
        {
            InitAttack();
            Canvas.SetLeft(AttackImage, Canvas.GetLeft(player.Sprite) + player.Sprite.Width / 2.9);
            Canvas.SetTop(AttackImage, player.Y);
            attackThread.Start();
        }

        private void InitAttack()
        {
            AttackImage.Width = 25;
            AttackImage.Height = 20;
            AttackImage.Fill = new ImageBrush(AttackVisual);
            RotateTransform rotateTransform = new RotateTransform(270);
            AttackImage.RenderTransform = rotateTransform;
        }

        private void StartAttack()
        {
            InvokeThread(() => _gameCanvas.Children.Add(AttackImage));

            int Distance = 2 * 45;
            int MoveDistanceAtATime = 5;
            for (int i = 0; i < Distance; i++)
            {

              
                InvokeThread(() =>
                Canvas.SetTop(AttackImage, Canvas.GetTop(AttackImage) - MoveDistanceAtATime));
                InvokeThread(() => AttackImage.Height += 0.3);
                InvokeThread(() => AttackImage.Width += 0.3);

                Thread.Sleep(20);
            }
            InvokeThread(() => _gameCanvas.Children.Remove(AttackImage));
        }
        private void InvokeThread(Action action)
        {
            try
            {
                _gameCanvas.Dispatcher.Invoke(action);
            }
            catch (Exception)
            {


            }

        }
    }
}
