﻿using SpaceInvaders.Life;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace SpaceInvaders.Managers
{
    class GameManager
    {
        public Canvas GameCanvas;
        private TimerManager _timerManager;
        public Player player;
        private MovmentManager _movementManager;
        public Map map;

        public GameManager(Canvas gameCanvas)
        {
            GameCanvas = gameCanvas;
            map = new Map(this);
            Enemy.InitEnemySprites();
            player = new Player(400, 350, gameCanvas);
            player.DrawSprite();
            _movementManager = new MovmentManager(this);
            _timerManager = new TimerManager(this, _movementManager);
            InitEnemies(gameCanvas);
        }

        private void InitEnemies(Canvas gameCanvas)
        {
            Random rnd = new Random();
            map.AddEnemy(new Enemy(100, 50, gameCanvas));
            map.Enemies.Last().DrawSprite();
            map.Enemies.Last().SetImage(rnd.Next(5));
            map.AddEnemy(new Enemy(250, 50, gameCanvas));
            map.Enemies.Last().DrawSprite();
            map.Enemies.Last().SetImage(rnd.Next(5));
            map.AddEnemy(new Enemy(400, 50, gameCanvas));
            map.Enemies.Last().DrawSprite();
            map.Enemies.Last().SetImage(rnd.Next(5));
            map.AddEnemy(new Enemy(550, 20, gameCanvas));
            map.Enemies.Last().DrawSprite();
            map.Enemies.Last().SetImage(rnd.Next(5));
        }

        internal void GameLoop_Tick(object sender, EventArgs e)
        {
            CheckForPlayerMove();
            CheckForAttacks();
        }

        private void CheckForAttacks()
        {
           
            if (Keyboard.IsKeyDown(Key.Space))
            {
                player.Attack();
            }
            if(player.attacks.Count != 0)
            map.CheckForAttackCollision();
            map.CheckForUserColision();
        }

        private void CheckForPlayerMove()
        {
            if (Keyboard.IsKeyDown(Key.Right))
            {
                player.RIGHT = true;
                player.LEFT = false;
            }
            else if (Keyboard.IsKeyDown(Key.Left))
            {
                player.LEFT = true;
                player.RIGHT = false;
            }
            else
            {
                player.LEFT = false;
                player.RIGHT = false;
            }
        }
        internal void Lost()
        {
            GameCanvas.Children.Remove(player.Sprite);
            _timerManager.GameLoop.Stop();
            _timerManager.MovmentLoop.Stop();
        }
    }
}
