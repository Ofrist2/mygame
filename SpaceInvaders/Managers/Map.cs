using SpaceInvaders.Inventory;
using SpaceInvaders.Life;
using System.Collections.Generic;

namespace SpaceInvaders.Managers
{
    internal class Map
    {
        private GameManager _gameManager;
        public List<Enemy> Enemies {get;set;}
        public List<Item> Items { get; set; }

        public Map(GameManager gameManager)
        {
            _gameManager = gameManager;
            Enemies = new List<Enemy>();
            Items = new List<Item>();
           
        }



        public void AddEnemy(Enemy enemy)
        {
            Enemies.Add(enemy);

        }
        public void CheckForAttackCollision()
        {
            Enemy RemoveEnemy = null;
            Attack RemoveAttack = null;
            bool innerBreak = false;
            foreach (var curAttack in _gameManager.player.attacks)
            {
                if (innerBreak) break;

                foreach (var enemy in Enemies)
                {

                    if (curAttack.IntersectWith(enemy.Sprite))
                    {
                        RemoveEnemy = enemy;
                        RemoveAttack = curAttack;
                        innerBreak = true;
                        break;
                    }

                }
            }
            if (RemoveEnemy != null)
            {
                SpawnItem(RemoveEnemy);
                Enemies.Remove(RemoveEnemy);
                _gameManager.player.attacks.Remove(RemoveAttack);
                _gameManager.GameCanvas.Children.Remove(RemoveAttack.AttackImage);
                _gameManager.GameCanvas.Children.Remove(RemoveEnemy.Sprite);
            }
        }
        private void SpawnItem(Enemy removeEnemy)
        {
            Item item = new Item(removeEnemy.X, removeEnemy.Y, _gameManager.GameCanvas);
            item.DrawSprite();
        }
        public void CheckForUserColision()
        {
            foreach (var enemy in Enemies)
            {
                if (_gameManager.player.IntersectWith(enemy.Sprite))
                {
                    _gameManager.Lost();
                }
            }
        }
    }
}