﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace SpaceInvaders.Managers
{
    class TimerManager
    {
        public DispatcherTimer GameLoop = new DispatcherTimer();
        public DispatcherTimer MovmentLoop = new DispatcherTimer();
        private GameManager _gameManager;
        private MovmentManager _movmentManager;

        public TimerManager(GameManager gameManager,MovmentManager movmentManager)
        {
            _gameManager = gameManager;
            _movmentManager = movmentManager;
            InitAllTimers();
        }
        private void InitAllTimers()
        {
            GameLoop.Tick += new EventHandler(_gameManager.GameLoop_Tick);
            GameLoop.Interval = new TimeSpan(0, 0, 0, 0, 10);
            GameLoop.Start();

            MovmentLoop.Tick += new EventHandler(_movmentManager.MovmentLoop_Tick);
            MovmentLoop.Interval = new TimeSpan(0, 0, 0, 0, 70);
            MovmentLoop.Start();

        }



    }
}
