﻿using System;
using System.Diagnostics;

namespace SpaceInvaders.Managers
{
    internal class MovmentManager
    {
        private GameManager gameManager;
        private double MAXIMUM_WIDTH = 805;
        private double MINIMUN_WIDTH = -5;

        public MovmentManager(GameManager gameManager)
        {
            this.gameManager = gameManager;
        }

        internal void MovmentLoop_Tick(object sender, EventArgs e)
        {
            if (gameManager.player.RIGHT)
            {
                if(gameManager.player.X < MAXIMUM_WIDTH - gameManager.player.MOVEMENT_SPEED)
                gameManager.player.X += gameManager.player.MOVEMENT_SPEED;
                Debug.Print(gameManager.player.X.ToString());
            }
            else if (gameManager.player.LEFT)
            {
                if (gameManager.player.X > MINIMUN_WIDTH + gameManager.player.MOVEMENT_SPEED)
                    gameManager.player.X -= gameManager.player.MOVEMENT_SPEED;
                Debug.Print(gameManager.player.X.ToString());
            }

            gameManager.map.Enemies.ForEach(x => x.Y += 5);

        }
    }
}