﻿using SpaceInvaders.Life;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;

namespace SpaceInvaders.Inventory
{
    class Item : GameObject
    {
        public Item(double x, double y, Canvas canvas) : base(x, y, canvas)
        {
            InitItemSprite();
        }
        private void InitItemSprite()
        {
            Sprite.Width = 32;
            Sprite.Height = 32;
            Sprite.Fill = new SolidColorBrush(Colors.Green);
        }
    }
}
