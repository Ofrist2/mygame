﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace SpaceInvaders.Life
{
    class GameObject
    {
        public double X { get => Canvas.GetLeft(Sprite); set => Canvas.SetLeft(Sprite, value); }
        public double Y { get => Canvas.GetTop(Sprite); set => Canvas.SetTop(Sprite, value);}
        public Canvas gameCanvas { get; }
        public Rectangle Sprite { get; set; }
        public GameObject(double x,double y,Canvas canvas)
        {
            Sprite = new System.Windows.Shapes.Rectangle();
            Canvas.SetLeft(Sprite, x);
            Canvas.SetTop(Sprite, y);
            gameCanvas = canvas;
            
        }
        public bool IntersectWith(Rectangle otherRect)
        {
            var SpriteBounds = new System.Drawing.Rectangle((int)Canvas.GetLeft(Sprite), (int)Canvas.GetTop(Sprite), (int)Sprite.Width, (int)Sprite.Height);
            var OtherBounds = new System.Drawing.Rectangle((int)Canvas.GetLeft(otherRect), (int)Canvas.GetTop(otherRect), (int)otherRect.Width, (int)otherRect.Height);
            return SpriteBounds.IntersectsWith(OtherBounds);

        }
        public void DrawSprite()
        {

            gameCanvas.Children.Add(Sprite);
        }
        public void MoveSprite()
        {
            
        }

    }
}
