﻿using SpaceInvaders.Managers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace SpaceInvaders.Life
{
    class Player : GameObject
    {
        public bool LEFT = false;
        public bool RIGHT = false;
        internal double MOVEMENT_SPEED;

        public List<Attack> attacks;
       // private AttackInfo attackInfo;
        private List<Thread> attackThread;
        private DateTime lastAttack;
        private object attObj = new object();

        

        public Player(double x, double y, Canvas canvas) : base(x, y, canvas)
        {
            InitPlayerSprite();
            attacks = new List<Attack>();
            MOVEMENT_SPEED = 35;
            //attackInfo = new AttackInfo(15, 15, 5,new SolidColorBrush(Colors.DarkRed));
            attackThread = new List<Thread>();

        }


        private void InitPlayerSprite()
        {
            Sprite.Width = 65;
            Sprite.Height = 70;
            Sprite.Fill = new ImageBrush(new BitmapImage(new Uri("pack://application:,,/Assets/SpaceShip.png")));
        }
        public void Attack()
        {

            DateTime now = DateTime.Now; // current time


            if (now > lastAttack)
            {
                lastAttack = DateTime.Now.AddMilliseconds(220);
                lock (attObj)
                {
                    attacks.Add(new Attack(gameCanvas));
                    attacks.Last().UseAttack(this);
                    Debug.Print(attacks.Count + "Shots!");
                }
            }
            else
            {

            }

        }


        private void InvokeThread(Action action)
        {
            gameCanvas.Dispatcher.Invoke(action);
        }

    }
}
