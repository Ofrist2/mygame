using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace SpaceInvaders.Life
{
    class Enemy : GameObject
    {
       private static Dictionary<int, BitmapImage> alians = new Dictionary<int, BitmapImage>();
        Random rnd = new Random();
        static int counter = 0;
        public Enemy(double x, double y, Canvas canvas) : base(x, y, canvas)
        {
            InitEnemySprite();
            
            
        }
       

        public static void InitEnemySprites()
        {
            alians.Add(alians.Count, new BitmapImage(new Uri("pack://application:,,/Assets/Enemy/first-alian.png")));
            alians.Add(alians.Count, new BitmapImage(new Uri("pack://application:,,/Assets/Enemy/second-alian.png")));
            alians.Add(alians.Count, new BitmapImage(new Uri("pack://application:,,/Assets/Enemy/third-alian.png")));
            alians.Add(alians.Count, new BitmapImage(new Uri("pack://application:,,/Assets/Enemy/4th-alian.jpg")));
            alians.Add(alians.Count, new BitmapImage(new Uri("pack://application:,,/Assets/Enemy/5th alian.png")));
        }
        public void SetImage(int val)
        {
            Sprite.Fill = new ImageBrush(alians[val]);
            
        }
        private void InitEnemySprite()
        {
            Sprite.Width = 85;
            Sprite.Height = 55;
            Sprite.Fill = new SolidColorBrush(Colors.Blue);
        }
    }
}
